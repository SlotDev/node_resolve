const mysql = require('mysql');
const dotenv = require('dotenv');
dotenv.config();

const conn = mysql.createConnection({
  host: process.env.HOST,
  database: process.env.BASENAME,
  user: process.env.USERNAME,
  password: process.env.PASSWORD,
  port: process.env.PORT,
  multipleStatements: true
});

conn.connect();

module.exports = { conn };
