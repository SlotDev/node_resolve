const { conn } = require('./config');

const toQuery = (stmt) => {
  return new Promise(async function(resolve, reject) {
    let sql = await conn.query(stmt, function (error, results, fields) {
      var data = results;
      resolve(data);
    });
  });
}

async function getResult(stmt) {
  let result = await toQuery(stmt);
  return result;  
}

async function doTask() {
  let queUsers = "SELECT * FROM users";
  let data = await getResult(queUsers);
  let resUser = data[1].firstname;

  let queWhereUser = `SELECT * FROM users WHERE firstname = '${resUser}'`;
  let data2 = await getResult(queWhereUser);
  console.log(data2);
}

doTask();




